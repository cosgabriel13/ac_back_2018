<?php

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$sex="";
$facebook = "";
//$facultate="";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text= "";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}


if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

/*if(isset($_POST['facultate'])){
	$facultate= $_POST['facultate'];
	}
*/
if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
if((strlen($firstname)<3 || strlen($firstname)>20)||(!preg_match ('/^([a-zA-Z]+)$/', $firstname)))
{
	$error=1;
	$error_text="Firstname has less than 3 characters or more than 20 or it contains a number";
}
if((strlen($lastname)<3 || strlen($lastname)>20)||(!preg_match ('/^([a-zA-Z]+)$/', $lastname)))
{
	$error=1;
	$error_text="lastname has less than 3 characters or more than 20 or it contains a number";
}
if(strlen($question)<15){
	$error=1;
	$error_text='question has less than 15 ch';
}

if(!is_numeric($phone)||strlen($phone)!=10){
	$error=1;
	$error_text="Phone number has a letter or it is smaller than 10";
}

if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
	$error=1;
	$error_text='Invalid email format';
}

if(strlen($cnp)!=13 || substr($cnp,-13,1)>6){
	$error=1;
	$error_text="cnp greater than 13 or the first letter is invalid";
}
if(preg_match('/^[1 3 5]/i',$cnp)){
	$sex="M";
}
if(preg_match('/^[2 4 6]/i',$cnp)){
	$sex="F";
}

/*if(is_numeric($facultate) || strlen($facultate) < 3 || strlen($facultate) > 30){
$error = 1;
$error_text = "Invalid format facultate";
}
*/
if(empty($firstname) || empty($lastname) || empty($phone)||empty($facebook) || empty($birth) || empty($department) || empty($question)|| empty($captcha_inserted)|| empty($check))
{
	$error=1;
	$error_text='One or more fields are empty';
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
if($error==0){
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,sex,facebook,birth,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:sex,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':sex',$sex);
$stmt2 -> bindParam(':facebook',$facebook);
//$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}
}
else{
	echo $error_text;
}
